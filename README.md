# Week 4_Luyện tập Form & Validation
Viết Flutter App có màn hình Login (gồm có email address và password). Trong đó thực hiện validation như sau:
- Email address: nếu không có dấu @ và dấu . thì báo lỗi 
- Password nếu không tuân thủ qui tắc sau thì báo lỗi:
  + Có ít nhất 1 kí tự Hoa.
  + Có ít nhất 1 kí tự thường.
  + Có ít nhất 1 kí tự đặc biệt.
  + Độ dài ít nhất là 8 kí tự


